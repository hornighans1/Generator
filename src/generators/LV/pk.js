const Generator = require('../../abstractGenerator');
const {randRange, diffYears} = require('../../helpers');
const handleParams = require('../../handleParams');

module.exports = class PK extends Generator {
    params() {
        return {
            format: {
                type: 'string',
                values: ['old', 'new'],
                default: 'new',
            },
            birthdate: {
                type: 'string',
                pattern: '^(18|19|20|21|22)[0-9][0-9](-[0-9][0-9])?(-[0-9][0-9])?$',
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        const nr = params.format === 'old'
            ? this._generateOld(params.birthdate)
            : '32' + randRange(0, 100000000).toString().padStart(8, '0');

        return nr.slice(0, 6) + '-' + nr.slice(6) + this._calcChecksum(nr);
    }

    _generateOld(birthdate) {
        let [y, m, d] = this._parseDateRequirement(birthdate);
        if (!y) {
            y = randRange(1900, 2020);
        }
        if (!m) {
            m = randRange(1, 13);
        }
        if (!d) {
            d = randRange(1, 29);
        }

        return d.toString().padStart(2, '0')
            + m.toString().padStart(2, '0')
            + (y % 100).toString().padStart(2, '0')
            + (parseInt(Math.floor(y / 100)) - 18).toString()
            + randRange(0, 1000).toString().padStart(3, '0');
    }

    validate(value) {
        value = value.replace(/-/g, '');

        if (!value.match(/^\d{11}$/g)) {
            throw new Error('format')
        }

        if (value.slice(0, 2) === '32') {
            if (this._calcChecksum(value.slice(0, 10)) !== parseInt(value[10])) {
                throw new Error('checksum')
            }

            return {};
        }

        let [d, m, y, c] = [
            parseInt(value.slice(0, 2)),
            parseInt(value.slice(2, 4)),
            parseInt(value.slice(4, 6)),
            parseInt(value.slice(6, 7)),
        ];

        if ((m > 12) || (d > 31) || (m === 0) || (d === 0)) {
            throw new Error('birthdate')
        }

        if (this._calcChecksum(value.slice(0, 10)) !== parseInt(value[10])) {
            throw new Error('checksum')
        }

        const birthdate = new Date(`${(c + 18) * 100 + y}-${m.toString().padStart(2, '0')}-${d.toString().padStart(2, '0')}T00:00:00Z`);

        return {
            birthdate,
            age: diffYears(new Date(), birthdate),
        };
    }

    _calcChecksum(nr) {
        const [a, b, c, d, e, f, x, g, h, i] = nr.split('');

        return (1101 - (1*a + 6*b + 3*c + 7*d + 9*e + 10*f + 5*x + 8*g + 4*h + 2*i)) % 11 % 10;
    }

    _parseDateRequirement(date) {
        if (date === '') {
            return [undefined, undefined, undefined];
        }

        if (date.match(/^\d{4}$/g)) {
            return [parseInt(date), undefined, undefined];
        }

        if (date.match(/^\d{4}-\d{2}$/g)) {
            return [parseInt(date.slice(0, 4)), parseInt(date.slice(5, 7)), undefined];
        }

        if (date.match(/^\d{4}-\d{2}-\d{2}$/g)) {
            return [parseInt(date.slice(0, 4)), parseInt(date.slice(5, 7)), parseInt(date.slice(8, 10))];
        }

        throw new Error('Invalid date requirement');
    }
};
